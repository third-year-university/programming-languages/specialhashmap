class SpecialHashMapException(Exception):
    ...


class SpecialHashMap(dict):
    @staticmethod
    def split(string):
        string = string.replace(" ", "")
        accepted_symbs = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '.', '<', '>', '=']
        array = []
        start_symb = 0
        for i in range(len(string)):
            if string[i] not in accepted_symbs:
                # print(string)
                array.append(string[start_symb:i])
                start_symb = i + 1
        array.append(string[start_symb:])
        return array

    sorted_keys = []
    additional_dict = dict()

    def __init__(self) -> None:
        super().__init__()
        self.unsorted = True

    def sort_keys(self):
        self.sorted_keys = sorted(super().keys())

    def __setitem__(self, key, value):
        self.unsorted = True
        my_key = key
        if my_key[0] == '(' and my_key[-1] == ')':
            my_key = my_key[1:-1]
        parts = SpecialHashMap.split(my_key)
        is_good = True
        for part in parts:
            try:
                float(part)
            except ValueError:
                is_good = False
        if is_good:
            self.additional_dict[tuple(map(float, parts))] = key
        return super().__setitem__(key, value)

    def clear(self):
        self.unsorted = True
        return super().clear()

    def pop(self, __key):
        self.unsorted = True
        return super().pop(__key)

    def popitem(self):
        self.unsorted = True
        return super().popitem()

    def update(self, __m, **kwargs):
        self.unsorted = True
        return super().update(__m, **kwargs)

    @property
    def iloc(self):
        return Iloc(self)

    @property
    def ploc(self):
        return Ploc(self)


class Iloc:
    def __init__(self, s: SpecialHashMap) -> None:
        self.s = s

    def __getitem__(self, item: int):
        if not isinstance(item, int):
            raise SpecialHashMapException("Param should be int")
        if item < 0:
            raise SpecialHashMapException("Param should be positive os zero")
        if self.s.unsorted:
            self.s.sort_keys()
        return self.s[self.s.sorted_keys[item]]


class Ploc:
    @staticmethod
    def more_or_equal(a, b):
        return a >= b

    @staticmethod
    def more(a, b):
        return a > b

    @staticmethod
    def less(a, b):
        return a < b

    @staticmethod
    def less_or_equal(a, b):
        return a <= b

    @staticmethod
    def equal(a, b):
        return a == b

    @staticmethod
    def not_equal(a, b):
        return a != b

    def __init__(self, s: SpecialHashMap) -> None:
        self.s = s

    def __getitem__(self, item):
        items = SpecialHashMap.split(item)
        actions = []
        my_nums = []
        for item in items:
            if item[0:2] == ">=":
                actions.append(Ploc.more_or_equal)
                try:
                    my_nums.append(float(item[2:]))
                except ValueError:
                    raise SpecialHashMapException("Wrong number")
            elif item[0:1] == ">":
                actions.append(Ploc.more)
                try:
                    my_nums.append(float(item[1:]))
                except ValueError:
                    raise SpecialHashMapException("Wrong number")
            elif item[0:2] == "<=":
                actions.append(Ploc.less_or_equal)
                try:
                    my_nums.append(float(item[2:]))
                except ValueError:
                    raise SpecialHashMapException("Wrong number")
            elif item[0:2] == "<>":
                actions.append(Ploc.not_equal)
                try:
                    my_nums.append(float(item[2:]))
                except ValueError:
                    raise SpecialHashMapException("Wrong number")
            elif item[0:1] == "<":
                actions.append(Ploc.less)
                try:
                    my_nums.append(float(item[1:]))
                except ValueError:
                    raise SpecialHashMapException("Wrong number")
            elif item[0:1] == "=":
                actions.append(Ploc.equal)
                try:
                    my_nums.append(float(item[1:]))
                except ValueError:
                    raise SpecialHashMapException("Wrong number")
            else:
                raise SpecialHashMapException("Condition not found")
        answer = dict()
        for nums in self.s.additional_dict:
            if len(items) != len(nums):
                continue
            all_good = True
            for i, num in enumerate(nums):
                all_good = all_good and actions[i](num, my_nums[i])
            if all_good:
                answer[nums] = self.s[self.s.additional_dict[nums]]
        return answer
