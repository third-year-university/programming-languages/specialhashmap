import pytest

from SpecialHashMap import SpecialHashMap, Iloc, Ploc, SpecialHashMapException


@pytest.fixture
def test_map() -> SpecialHashMap:
    my_map = SpecialHashMap()
    my_map["value1"] = 1
    my_map["value2"] = 2
    my_map["value3"] = 3
    my_map["1"] = 10
    my_map["2"] = 20
    my_map["3"] = 30
    my_map["1, 5"] = 100
    my_map["5, 5"] = 200
    my_map["10, 5"] = 300
    return my_map


@pytest.fixture
def test_map2() -> SpecialHashMap:
    my_map = SpecialHashMap()
    my_map["value1"] = 1
    my_map["value2"] = 2
    my_map["value3"] = 3
    my_map["1"] = 10
    my_map["2"] = 20
    my_map["3"] = 30
    my_map["(1, 5)"] = 100
    my_map["(5, 5)"] = 200
    my_map["(10, 5)"] = 300
    my_map["(1, 5, 3)"] = 400
    my_map["(5, 5, 4)"] = 500
    my_map["(10, 5, 5)"] = 600
    return my_map


class TestSpecialHashMap():
    def test_requests(self, test_map2):
        assert test_map2.ploc["<>1"] == {(2,): 20, (3, ): 30}
        assert test_map2.ploc["=1"] == {(1,): 10}
        assert test_map2.ploc["<=1"] == {(1,): 10}


    @pytest.mark.parametrize(
        "key, val",
        [(0, 10),
         (2, 300),
         (5, 200),
         (8, 3)
         ]
    )
    def test_getting_iloc(self, key, val, test_map):
        assert test_map.iloc[key] == val

    def test_split(self):
        assert SpecialHashMap.split("1.2 , 2 | 234 | 453, 753, <=2 ?? 9") == ['1.2', '2', '234', '453', '753', "<=2",
                                                                              '', '9']

    def test_eq(self):
        assert Ploc.equal(1, 1)
        assert not Ploc.equal(1, 2)

    def test_n_eq(self):
        assert Ploc.not_equal(1, 2)
        assert not Ploc.not_equal(1, 1)

    def test_le(self):
        assert Ploc.less_or_equal(1, 2)
        assert Ploc.less_or_equal(1, 1)
        assert not Ploc.less_or_equal(2, 1)

    def test_l(self):
        assert Ploc.less(1, 2)
        assert not Ploc.less(1, 1)
        assert not Ploc.less(2, 1)

    def test_me(self):
        assert not Ploc.more_or_equal(1, 2)
        assert Ploc.more_or_equal(1, 1)
        assert Ploc.more_or_equal(2, 1)

    def test_m(self):
        assert not Ploc.more(1, 2)
        assert not Ploc.more(1, 1)
        assert Ploc.more(2, 1)

    @pytest.mark.parametrize(
        "key, val",
        [(">=1", {(1,): 10, (2,): 20, (3,): 30}),
         ("<3", {(1,): 10, (2,): 20}),
         (">0, >0", {(1, 5): 100, (5, 5): 200, (10, 5): 300}),
         (">=10, >0", {(10, 5): 300}),
         ("<5, >=5, >=3", {(1, 5, 3): 400})

         ]
    )
    def test_getting_iloc(self, key, val, test_map2):
        assert test_map2.ploc[key] == val

    def test_empty(self, test_map):
        test_map.clear()
        with pytest.raises(IndexError):
            test_map.iloc[0]

    def test_add_keys(self, test_map):
        lol = test_map.iloc[0]
        test_map['0'] = 9999
        assert test_map.iloc[0] == 9999

    def test_pop(self, test_map):
        val = test_map.iloc[len(test_map) - 1]
        test_map.pop('value3')
        assert val != test_map.iloc[len(test_map) - 1]

    def test_pop_item(self, test_map):
        p = test_map.popitem()
        for i in range(len(test_map)):
            assert test_map.iloc[i] != p[1]

    def test_update(self, test_map):
        test_map.update({'0': 111111111})
        assert test_map.iloc[0] == 111111111

    def test_exceptions1(self, test_map):
        with pytest.raises(SpecialHashMapException):
            test_map.iloc["1"]
        with pytest.raises(SpecialHashMapException):
            test_map.iloc[-1]


    def test_exceptions_2(self, test_map2):
        with pytest.raises(SpecialHashMapException):
            test_map2.ploc[">=adsadsa"]
        with pytest.raises(SpecialHashMapException):
            test_map2.ploc[">adsadsa"]
        with pytest.raises(SpecialHashMapException):
            test_map2.ploc["<=adsadsa"]
        with pytest.raises(SpecialHashMapException):
            test_map2.ploc["<>adsadsa"]
        with pytest.raises(SpecialHashMapException):
            test_map2.ploc["<adsadsa"]
        with pytest.raises(SpecialHashMapException):
            test_map2.ploc["=qwewqeqe"]
        with pytest.raises(SpecialHashMapException):
            test_map2.ploc["asdafasd"]
        with pytest.raises(SpecialHashMapException):
            test_map2.ploc["sdad<=asdafasd"]
